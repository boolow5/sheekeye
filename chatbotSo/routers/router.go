package routers

import (
	"bitbucket.org/boolow5/sheekeye/chatbotSo/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})
}
