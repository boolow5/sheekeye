package models


type (
  Word struct {
    Id int `json:"id"`
    Text string `json:"text"`
    Related []int `json:"related"`
    RelatedWords []interface{} `json:"related_words"`
    UsageFrequency int `json:"usage_frequency"`
  }

  Sentence struct {
    Id int `json:"id"`
    RawText string `json:"raw_text"`
    Words []Word `json:"words"`
    Type string
  }

  Speech struct {
    Id int `json:"id"`
    Sentences []Sentence `json:"sentences"`
    SpeakerId int `json:"speaker_id"`
  }

  Chat struct {
    Id int `json:"id"`
    Conversation []Speech `json:'conversation'`
    MemberIds []int `json:"member_ids"`
  }
)
